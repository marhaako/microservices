# Microservices example
Author: Marius H�konsen 
(https://bitbucket.org/marhaako)


Just a very simple example for a paper on how to create microservices.

Created using Spring-Boot and Java.

## Diagram for example of usage
In the following link you can find a diagram showing you examples of how these microservices can be used
in a booking system for restaurants. 
https://drive.google.com/file/d/1Ib1Ddj6wEAVY6SYIZPDhNWpsBwZY1sVg/view?usp=sharing
