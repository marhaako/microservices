package test;

import java.util.ArrayList;

public class RestaurantDB {
    ArrayList<Restaurant> restaurants;

    public RestaurantDB() {
        restaurants = new ArrayList<>();
    }

    public void add(Restaurant restaurant) {
        restaurants.add(restaurant);
    }

    public ArrayList getAllRestaurants() {
        return restaurants;
    }

    public Restaurant getRestaurantWithID(int id) {
        for(Restaurant rest : restaurants) {
            if(rest.getId() == id) {
                return rest;
            }
        }
        return null;
    }
}
