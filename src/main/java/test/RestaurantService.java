package test;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class RestaurantService {

    private static final String template = "Hello, %s";
    private final AtomicInteger restID= new AtomicInteger();
    private RestaurantDB restaurantDB = new RestaurantDB();

    public RestaurantService() {
        buildMockDB();
    }

    private void buildMockDB() {
        restaurantDB.add(new Restaurant(restID.incrementAndGet(), "Jako Sushi", "Hunnsvegen 7", "61181607", "jako@sushi.no", "Sushi"));
        restaurantDB.add(new Restaurant(restID.incrementAndGet(), "Egon", "Jernbanegata 5", "61132410", "egon@egon.no", "Matservering"));
        restaurantDB.add(new Restaurant(restID.incrementAndGet(), "Panda Restaurant", "Strandgata 13", "61132668", "panda@restaurant.no", "Koselig, Uformelt, Barnevennlig"));
        restaurantDB.add(new Restaurant(restID.incrementAndGet(), "Salt & Pepper", "Bakkegata 4", "61132122", "salt@pepper.no", "Uteservering, Matservering"));

        System.out.println("Mock RestaurantDB built");
    }


    /**
     *
     * @return
     */
    @RequestMapping(method=GET, value="/restaurants")
    public ArrayList allRestaurants() {
        return (restaurantDB.getAllRestaurants());
    }

    @RequestMapping(method=GET, value="/restaurants/restaurant")
    public Restaurant restaurantByID(@RequestParam(value="restID", defaultValue="0") int restID) {
        return (restaurantDB.getRestaurantWithID(restID));
    }

    @RequestMapping(method=POST, value="/restaurants/restaurant")
    public Restaurant newRestaurant(@RequestParam(value="restName") String restName,
                                 @RequestParam(value="restAddress") String restAddress,
                                 @RequestParam(value="restPhone") String restPhone,
                                 @RequestParam(value="restMail") String restMail,
                                 @RequestParam(value="restDescription") String restDescription) {

        restaurantDB.add(new Restaurant(restID.incrementAndGet(), restName, restAddress, restPhone, restMail, restDescription));

        return (restaurantDB.getRestaurantWithID(Integer.parseInt(restID.toString())));
    }


}
