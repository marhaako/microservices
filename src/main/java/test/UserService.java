package test;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class UserService {

    private static final String template = "Hello, %s";
    private final AtomicInteger userID= new AtomicInteger();
    private UserDB userDB = new UserDB();

    public UserService() {
        buildMockDB();
    }

    private void buildMockDB() {
        userDB.add(new User(userID.incrementAndGet(), "John", "Johnsen", "male", "example@ex.com", "95566123"));
        userDB.add(new User(userID.incrementAndGet(), "Peter", "Barns", "male", "example1@ex1.com", "92223123"));
        userDB.add(new User(userID.incrementAndGet(), "Richard", "Stephens", "male", "sparkles@ex.com", "94452344"));
        userDB.add(new User(userID.incrementAndGet(), "Violetta", "Brown", "female", "rice@rice.com", "99087463"));

        System.out.println("Mock UserDB built");
    }


    @RequestMapping(method=GET, value="/users")
    public ArrayList allUsers() {
        return (userDB.getAllUsers());
    }

    @RequestMapping(method=GET, value="/users/user")
    public User user(@RequestParam(value="userID", defaultValue="0") int userID) {
        return (userDB.getUserWithID(userID));
    }


}
