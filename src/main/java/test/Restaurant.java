package test;

public class Restaurant {
    private int id;
    private String name;
    private String address;
    private String phone;
    private String email;
    private String description;


    public Restaurant(int id, String name, String address, String phone, String email, String description) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
