package test;


public class Booking {
    private int bookingID;
    private int restaurantID;
    private int userID;
    private String dateTime;
    private int seats;
    private float price;


    public Booking(int bookingID, int userID, int restaurantID, String dateTime, int seats, float price) {
        this.bookingID = bookingID;
        this.userID = userID;
        this.restaurantID = restaurantID;
        this.dateTime = dateTime;
        this.seats = seats;
        this.price = price;
    }

    public String getDateTime() {
        return dateTime;
    }

    public int getSeats() {
        return seats;
    }


    public int getRestaurantID() {
        return restaurantID;
    }

    public int getUserID() {
        return userID;
    }

    public int getBookingId() {
        return bookingID;
    }

    public float getPrice() {
        return price;
    }
}
