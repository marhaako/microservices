package test;

import java.awt.print.Book;
import java.util.ArrayList;

public class User {
    private int id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private String phone;

    public User(int id, String firstName, String lastName, String gender, String email, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }

}
