package test;

import java.util.ArrayList;

public class UserDB {
    ArrayList<User> users;
    public UserDB() {
        users = new ArrayList<>();
    }

    public void add(User user) {
        users.add(user);
    }

    public ArrayList getAllUsers() {
        return users;
    }

    public User getUserWithID(int id) {
        for(User user : users) {
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}
