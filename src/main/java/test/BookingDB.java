package test;

import java.util.ArrayList;

public class BookingDB {
    ArrayList<Booking> bookings;

    public BookingDB() {
        bookings = new ArrayList<>();
    }

    public void add(Booking booking) {
        bookings.add(booking);
    }

    public ArrayList getAllBookings() {
        return bookings;
    }

    public Booking getBookingWithID(int id) {
        for(Booking booking : bookings) {
            if(booking.getBookingId() == id) {
                return booking;
            }
        }
        return null;
    }

    public ArrayList getBookingsForUserID(int id) {
        ArrayList<Booking> bookingList = new ArrayList<>();
        for(Booking booking : bookings) {
            if(booking.getUserID() == id) {
                bookingList.add(booking);
                System.out.println(booking.getUserID());
            }
        }
        return bookingList;
    }

    public ArrayList getBookingsForRestaurantID(int id) {
        ArrayList<Booking> bookingList = new ArrayList<>();
        for(Booking booking : bookings) {
            if(booking.getRestaurantID() == id) {
                bookingList.add(booking);
            }
        }
        return bookingList;
    }
}
