package test;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class BookingService {

    private final AtomicInteger bookingID= new AtomicInteger();
    private BookingDB bookingDB = new BookingDB();

    public BookingService() {
        buildMockDB();
    }

    private void buildMockDB() {

        System.out.println("Mock BookingDB built");
    }



    @RequestMapping(method=GET, value="/bookings/restaurant")
    public ArrayList getBookingsForRestaurantID(@RequestParam(value="restID", defaultValue="0") int restID) {
        return (bookingDB.getBookingsForRestaurantID(restID));
    }

    @RequestMapping(method=GET, value="/bookings/user")
    public ArrayList getBookingForUserID(@RequestParam(value="userID", defaultValue="0") int userID) {
        return (bookingDB.getBookingsForUserID(userID));
    }

    @RequestMapping(method=POST, value="/bookings")
    public Booking newBooking(@RequestParam(value="restaurantID") int restaurantID,
                                    @RequestParam(value="userID") int userID,
                                    @RequestParam(value="dateTime") String dateTime,
                                    @RequestParam(value="seats") int seats,
                                    @RequestParam(value="price") float price) {

        bookingDB.add(new Booking(bookingID.incrementAndGet(), userID, restaurantID, dateTime, seats, price));

        return (bookingDB.getBookingWithID(Integer.parseInt(bookingID.toString())));
    }

}
